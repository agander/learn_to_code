module Main where

import qualified MyLib (someFunc)

-- | sack weights
sack_weights :: [Double]
sack_weights = [25.75,29.45,28.59,31.14,27.97,34.63,28.01,29.2,29.8,27.57,29.19,26.65,33.33,32.56,34.22,34.25]



main :: IO ()
main = do
  putStrLn "Hello, Haskell!"
  MyLib.someFunc
