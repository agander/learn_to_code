Purpose of this exercise
########################

Detail
~~~~~~

This is a small project/exercise whose purpose is to work:

1. through some analysis of an issue;
2. to complete the task(s) quickly;
3. to work those communication skills:
   a) are there any side issues?
   b) are there any tests?/does it need any tests?;
4. to practise working with lists and list comprehensions;
5. to use skills learnt;

Task
----

You are given a list of floats.
They are the weights of 16 sacks of almonds.
They are not all the same weight.

The owner of the field will get 50%.
The pickers will get the other 50%.
This is the usuall Sicilian divvy-up.

You need to print out the weights that satisfy the 50/50 rule.
The weights may not divvy up 50/50 equally, 
so this situation should be printed out.

