# What is bash

`bash` is a program (binary, executable) which allows one to interact with a Linux computer. As of 2020 it is also possible on native windows (WSL).

Once started, it displays some information, such as the current directory (known as a prompt), before a cursor.

You then enter commands to commence interaction.

When the command finishes, its output will be displayed or any error(s) that may have occured.


```
gander@tagander:~ $
> date
Sat Jul 18 14:19:39 CEST 2020
gander@tagander:~ $
> 
```

In the above snippet, I have entered the `date` command.
In its default mode of operation, it didnt require any arguments, it just displayed the current date & time (as known by the machine).

`date` is just another executable (or binary) like `bash`.


`bash`, the program, is a toolbox.
It helps you write scripts.
It runs other program, as in `date` above, and then manages their output.

It also has piping.

This is where 2 commands are 'joined' together with the `|` (pipe) character.
See 	


# Directory Structure

The Linux Standard is:
ref. [File System Heirarchy Standard](https://refspecs.linuxfoundation.org/FHS_2.3/fhs-2.3.html)

`/` : The 'root' of the system. The root user also uses this name. See `/root`

`/usr` : holds executables and libraries

`/etc` : this dir keeps general configuration files for installed programs

`/tmp` : temporary files used by everyone.

`/var` : data files used by all

`/home`: User home directories

`/proc`: process and file information

`/dev` : device files: camera, microfono

`/boot` : files used to boot the system

`/root` : the home directory of the administrator user.
          The structure in here will, largely be the same as a non-root user.

`/opt` : chrome gets installed here

Drilling down inside some of the above, we get:

`/usr/bin, /usr/sbin` : `bin` signifies binary or executable. Prefixed with an 's' means
'system', meaning they are programs which may only run by the administrator (SysAdm).

macOS is slightly different.
The [details](https://osxdaily.com/2007/03/30/mac-os-x-directory-structure-explained/) say:

`/Applications` : self explanatory, this is where your Mac’s applications are kept
`/Developer` : the Developer directory appears only if you have installed Apple’s Developer Tools, 
and no surprise, contains developer related tools, documentation, and files.
`/Library` : shared libraries, files necessary for the operating system to function properly, 
including settings, preferences, and other necessities  (note: you also have 
a Libraries folder in your home directory, which holds files specific to that user).
`/Network` : largely self explanatory, network related devices, servers, libraries, etc
`/System` : system related files, libraries, preferences, critical for the proper function of Mac OS X
`/Users` : all user accounts on the machine and their accompanying unique files, settings, etc. Much like /home in Linux
`/Volumes` : mounted devices and volumes, either virtual or real, such as hard disks, CD’s, DVD’s, DMG mounts, etc




### The command line and prompt

`gander@tagander:~$ `

The command line prompt looks something like the above.
'gander' is the user name - a short code to reference the person 
owning this login session.
'tagander' is the hostname of the machine you are connected to.
The '@' is used by convention to say: "This person logged into this machine".

The ':' is a separator.
The '~' is another way to refer to the 'home' directory.
The '$' is by convention, a familiar marker to say: 
"Everything you type will always be after this $ sign. Everything before it is programmable".

You can personalise any part of the prompt as you can see from the example in the 1st paragraph.


#### No [That is the way I have set mine - the '$' has become:]
`
> `




### Running a command


### Help

The original unix manual is called `man`.
There is `info` as well.

Both have been used for a long time but are not beginner friendly.

Recently (when ?) 

Recent (when ?) versions of `bash` include `help`.

### Piping input and/or output and/or errors

When you call a program, for instance `date` it prints the output to stdout:


### Logging in

On most Linux boxes, there are 2 ways login.

1. via a virtual console or terminal;
2. Via the gui - think Mac

The original Unix only had a green text on black background console.
There was no GUI.




### Permissions

To list the contents of a directory one can use the `ls` command.
On its own, without any parameters (the default operation), `ls`, produces:
```
rand.sh
tmp
```

### Administration/su/root







