##############
Learn To Code
##############

Purpose of this Course
----------------------

Learning about Linux is both satisfying and rewarding but may end up not
paying the bills on its own. Learning about Windows is both torture and
unrewarding but may pay the bills.

`bash <docs/bash.md>`__
~~~~~~~~~~~~~~~~~~~~~~~

`git <docs/git.md>`__
~~~~~~~~~~~~~~~~~~~~~

`python?`__
~~~~~~~~~~~

`Haskell <docs/haskell.md>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`glossary <docs/glossary.md>`__
-------------------------------

`How do I… <docs/howdoi.md>`__
------------------------------

Editing/Editors
---------------

Integrated Development Environment (IDE)
----------------------------------------

What this course does Not teach
-------------------------------

.cabal .html, .css, Makefile syntaxes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As far as possible, we will use tools to generate these files and
others.

e.g. `stack/cabal`__ for `.cabal`__, `automake`__ for a `Makefile`__, Lucid/Blaze for `html`__.

So the learning curve will be in the syntax for the configuration for the tool instead.

